var dataSrc = [
	{
        "text": "Recentes", 
        "iconCls": "fa fa-history"
    },
    {
        "text": "Home", 
        "iconCls": "fa fa-home"
    },
    {
        "text": "Documentos",
        "iconCls": "fa fa-file-text",
        "items": [
            {
                "text": "Meus WebSites",
                "iconCls": "fa fa-folder"
            }
        ]
    },
    {
        "text": "Fotos",
        "iconCls": "fa fa-picture-o",
        "items": [
            {
                "text": "Camera",
                "iconCls": "fa fa-folder"
            },
            {
                "text": "Screenshoots",
                "iconCls": "fa fa-folder"
            }
        ]
    },
    {
        "text": "Músicas",
        "iconCls": "fa fa-music"
    },
    {
        "text": "Downloads",
        "iconCls": "fa fa-arrow-down"
    },
    {
        "text": "Video",
        "iconCls": "fa fa-film",
        "items": [
            {
                "text": "aulas-gravadas",
                "iconCls": "fa fa-folder"
            }
        ]
    },
    {
        "text": "Lixeira",
        "iconCls": "fa fa-trash-o"
    },
    {
        "text": "sw-san",
        "iconCls": "fa fa-user",
        "cls": "custom"
    },
    {
        "text": "Torrents",
        "iconCls": "fa fa-folder"
    },
    {
        "text": "aulas",
        "iconCls": "fa fa-folder"
    },
]

var folderData = 
[
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">", 
        "text": "bin"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "boot"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "cdrom"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "dev"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "etc"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "home"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "lib32"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "lib64"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "system32"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/forbidden-folder.png\">",
        "text": "run"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "sbin"
    }, 
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "temp"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "opt"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/folder.png\">",
        "text": "media"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/iso-icon.png\">",
        "text": "initrd.img"
    },
    {
        "icon": "<img class=\"img-responsive\" src=\"/Content/images/template/filemanager/exe-icon.png\">",
        "text": "vm.exe"
    },
];

