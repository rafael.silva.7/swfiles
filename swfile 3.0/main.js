document.getElementById("login-form").addEventListener("submit", function (e){
    e.preventDefault();

    var username = document.querySelector('input[name="usuario"]').value;
    var password = document.querySelector('input[name="senha"]').value;

    // Verifica se os campos estão vazios
    if (!username || !password) {
        showError("Por favor, preencha ambos os campos.");
        return;
    }

    // Verifica as credenciais
    if (username === "admin" && password === "0000") {
        showSuccess("Login bem-sucedido!");
        window.location.href = "./gestor/gestor.html";
    } 
    else {
        showError("Login falhou. Verifique seu usuário e senha.");
    }
});

// Função para exibir mensagem de sucesso
function showSuccess(message) {
    var successMessage = document.createElement('div');
    successMessage.className = 'success-message';
    successMessage.textContent = message;

    var loginBox = document.querySelector('.login-box');
    loginBox.appendChild(successMessage);

    // Remove a mensagem de sucesso após 3 segundos
    setTimeout(function () {
        successMessage.remove();
    }, 3000);
}

// Função para exibir mensagem de erro
function showError(message) {
    var errorMessage = document.createElement('div');
    errorMessage.className = 'error-message';
    errorMessage.textContent = message;

    var loginBox = document.querySelector('.login-box');
    loginBox.appendChild(errorMessage);

    // Remove a mensagem de erro após 3 segundos
    setTimeout(function () {
        errorMessage.remove();
    }, 3000);
}
